CC=gcc
CFLAGS=-lm -fPIC
#CFLAGS=-pedantic -Wall -Werror -std=c99 
CFLAGS+=-g
#CFLAGS+=-march=native
#CFLAGS+=-o2

all: sharedlib bin/fizziks_test

test: bin/fizziks_test
	bin/fizziks_test

test_v: bin/fizziks_test
	valgrind --leak-check=full bin/fizziks_test

bin/fizziks_test: src/fizziks_test.c src/fizziks_core.c
	$(CC) $(CFLAGS) src/fizziks_core.c src/fizziks_test.c -o bin/fizziks_test

#testGui: src/fizziks_gui_test.c src/fizziks_core.o
#	$(CC) $(CFLAGS) fizziks_core.o fizziks_gui_test.c -o bin/fizziks_gui_test

sharedlib: ./lib/libfizziks.so

lib/libfizziks.so: src/fizziks_core.o
	$(CC) $(CFLAGS) -shared src/fizziks_core.o -o lib/libfizziks.so

src/fizziks_core.o: src/fizziks_core.c src/fizziks_core.h
	$(CC) $(CFLAGS) -c src/fizziks_core.c -o src/fizziks_core.o

clean:
	rm lib/*.so || true
	rm bin/* || true
	rm src/*.o || true