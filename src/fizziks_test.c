#include <stdio.h>
#include "fizziks_core.h"

int test ( void );

int main ( void ) {
	return test();
}

int test ( void ) {

	if (init(1.0) != 0) {
		printf("failed to initialize fizziks\n");
		return 1;
	}
	printf("successful init!\n");

	int first_shape = start_shape();
	if (first_shape == -1) {
		printf("error creating first shape\n");
		return 1;
	}
	printf("adding points to shape number %d\n",first_shape);
	add_point_to_shape(first_shape,-1.0,-1.0);
	add_point_to_shape(first_shape,-1.0,1.0);
	add_point_to_shape(first_shape,1.0,1.0);
	add_point_to_shape(first_shape,1.0,-1.0);
	printf("all points added to shape.\n");

	printf("creating new shape\n");
	int second_shape = start_shape();
	printf("shape number %d created\n",second_shape);

        printf("adding points to shape number %d\n",second_shape);
        add_point_to_shape(second_shape,-2.0,-0.5);
        add_point_to_shape(second_shape,-2.0,0.5);
        add_point_to_shape(second_shape,2.0,0.5);
        add_point_to_shape(second_shape,2.0,-0.5);
        printf("all points added to shape.\n");

	printf("first test complete\n");

	printf("creating new object from shape %d\n",first_shape);
	int first_object = create_object(first_shape);
	if (first_object == -1) {
		printf("error creating first object\n");
		return 1;
	}
	printf("object %d created\n",first_object);
	printf("creating object from shape %d\n",second_shape);
	int second_object = create_object(second_shape);
	if (second_object == -1) {
		printf("error creating second object\n");
		return 1;
	}
	printf("object number %d created\n",second_object);

        printf("setting object mass\n");

        set_object_mass(first_shape,1);
        set_object_mass(second_shape,2);

        printf("moving shape 2\n");
        set_object_location(second_shape,0,1.5);
	set_object_velocity(second_shape,0,-0.5);

	printf("spawning additional objects\n");
	int spawning;
	int current;
	for (spawning = 0; spawning < 10; spawning++) {
		printf("%d object\n",spawning);
		current = create_object(first_shape);
		set_object_mass(current,1);
		set_object_location(current,0,-1 + spawning*-2);
	}

	int seconds;
	for (seconds = 0; seconds < 60; seconds++) {
	printf("interval #%d\n",seconds);
	do_tick_in_rect(100,-500,-500,1000,1000);
	printf("Tick processed\n");
	collision * collision_item;
	int collisions = 0;

	get_object_collisions(&collision_item);

	printf("counting collisions\n");
	while (collision_item != NULL) {
		collisions++;
		printf("%d overlapping points\n",collision_item->count);
		collision_item = collision_item->next;

	}

	printf("%d collisions detected\n",collisions);

	float * loc = calloc(2,sizeof(float));
	float * vel = calloc(2,sizeof(float));
	if (loc == NULL || vel == NULL) {
		printf("failed to allocate memory\n");
	}
	get_object_location(first_object,loc);
	get_object_velocity(first_object,vel);
	printf("object #1 is now at %f,%f\n",loc[0],loc[1]);
	printf("moving at %f,%f\n",vel[0],vel[1]);

	get_object_location(second_object,loc);
	get_object_velocity(second_object,vel);
        printf("object #2 is now at %f,%f\n",loc[0],loc[1]);
	printf("moving at %f,%f\n",vel[0],vel[1]);

	free(loc);
	free(vel);
	//sleep(1);
	}
}
