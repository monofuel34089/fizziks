#include "fizziks_core.h"

#define PI 3.14159f

//array of pointers for each object
//that can be destroyed and re-made for additional
//objects on the fly. kinda.
//link list would be optimal for adding/removing objects, however
//this way allows for easy random access.
shape ** shape_array;
int shape_array_size;
object ** object_array;
int object_array_size;
int * empty_slots;
int empty_size = 0;

//pretend array in contigious memory
//to detect for collisions
object * pretend_object_array;
int pretend_array_size = 0;

//linkedlist of collisions from the pretend step
collision * collision_first = (void *) NULL;
collision * collision_item = (void *) NULL;
collision * collision_item_end = (void *) NULL;

int global_scale;

int init(float scale) {
	global_scale = scale;
	shape_array_size = 0;
	object_array_size = 0;
	shape_array = (void *) 0;
	object_array = (void *) 0;

	return 0;
}

int check_id(int object_id) {
	if (object_id >= object_array_size) {
		printf("error: attempted access to nonexistant object!");
		return 0;
	}
	return 1;
}

void get_object_collisions(collision ** collision_first_return) {

	*collision_first_return = collision_first;

}

//time in millis and bounds for simulation
//if off_x/y and w/h are 0, then simulate all
void do_tick_in_rect(int time, float off_x, float off_y,
				  float width, float height) {

	printf("processing pretend tick\n");
	//process 'pretend' tick
	pretend_tick(time,off_x,off_y,width,height);

	printf("detecting overlaps\n");
	//detect overlap points
	detect_overlaps();

	printf("calculating forces\n");
	//calculate forces and
	//apply counter-forces
	calc_forces();

	printf("apply real tick\n");
	//process real tick
	real_tick(time, off_x, off_y, width, height);
}

void pretend_tick(int time, float off_x, float off_y,
			    float width, float height) {

	//find all the objects in the specified rect
	//and add them to the pretend list
	int count = 0;
	int index;
	//printf("object array size %d\n",object_array_size);
	for (index = 0; index < object_array_size; index++) {
		if (object_array[index]->loc.x > off_x && object_array[index]->loc.x < height) {
			if (object_array[index]->loc.y > off_y && object_array[index]->loc.y < width) {
				count++;
			}
		}
	}
	//printf("found %d objects within rect\n",count);
	pretend_object_array = calloc(count,sizeof(object));
	count = 0;
	for (index = 0; index < object_array_size; index++) {
		if (object_array[index]->loc.x > off_x && object_array[index]->loc.x < height) {
			    if (object_array[index]->loc.y > off_y && object_array[index]->loc.y < width) {
				memcpy(&pretend_object_array[count],object_array[index],sizeof(object));
				count++;
			    }
		 }
	}
	pretend_array_size = count;

	//for all the objects in the pretend list
	//apply a pretend 'tick' to them

	float milliseconds = time/1000.0;

	for (index = 0; index < count; index++) {
		apply_tick(milliseconds,&pretend_object_array[index]);
	}
}

void real_tick(int time, float off_x, float off_y,
			 float width, float height) {
        //find all the objects in the specified rect
        //and add them to the pretend list
        float milliseconds = time/1000.0;
        int index;

        for (index = 0; index < object_array_size; index++) {
                if (object_array[index]->loc.x > off_x && object_array[index]->loc.x < height) {
                        if (object_array[index]->loc.y > off_y && object_array[index]->loc.y < width) {
                                apply_tick(milliseconds,object_array[index]);
                        }
                }
        }
}

void detect_overlaps() {

	//for each item in the pretend list
	//compare it with each next item
	//for overlap points.
	//TODO: free previous collisions
	//ha ha. no srs. real srs.
	//ram leak right here.

	collision_first = (void *) NULL;
	collision_item = (void *) NULL;
	collision_item_end = (void *) NULL;


	int index;
	for (index = 0; index < pretend_array_size; index++) {
		int index2;
		for (index2 = index+1; index2 < pretend_array_size; index2++) {

			overlap_compare(&pretend_object_array[index],&pretend_object_array[index2]);
			//create a link list of objects
			//for each collision storing the
			//amount of points, objects involved
			//and the overlapping points.
		}
	}

}

void overlap_compare(object * item1, object * item2) {

	/*create a link list of objects
	 *for each collision storing the
	 *amount of points, objects involved
	 *and the overlapping points.
	 */

	collision * final = calloc(1,sizeof(collision));
	final->object1 = item1;
	final->object2 = item2;


	shape * shape1;
	shape * shape2;

	shape1 = shape_array[item1->object_shape];
	shape2 = shape_array[item2->object_shape];


	int index, index2;

	for (index = 0; index < shape1->size; index++) {
		vec line1_start = shape1->points[index];
		vec line1_end;
		if (index == shape1->size - 1) {
			line1_end = shape1->points[0];
		} else {
			line1_end = shape1->points[index + 1];
		}

		for (index2 = 0; index2 < shape2->size; index2++) {
			vec line2_start = shape2->points[index2];
			vec line2_end;
			if (index2 == shape2->size -1) {
				line2_end = shape2->points[0];
			} else {
				line2_end = shape2->points[index2 + 1];
			}


			//apply rotation and translation

			float x1 = (line1_start.x * cos(item1->rotation) -
					line1_start.y * sin(item1->rotation)) + item1->loc.x;
			float x2 = (line1_end.x * cos(item1->rotation) -
					line1_end.y * sin(item1->rotation)) + item1->loc.x;
			float x3 = (line2_start.x * cos(item2->rotation) -
					line2_start.y * sin(item2->rotation)) + item2->loc.x;
			float x4 = (line2_end.x * cos(item2->rotation) -
					line2_end.y * sin(item2->rotation)) + item2->loc.x;

                        float y1 = (line1_start.x * sin(item1->rotation) +
                                        line1_start.y * cos(item1->rotation)) + item1->loc.y;
                        float y2 = (line1_end.x * sin(item1->rotation) +
                                        line1_end.y * cos(item1->rotation)) + item1->loc.y;
                        float y3 = (line2_start.x * sin(item2->rotation) +
                                        line2_start.y * cos(item2->rotation)) + item2->loc.y;
                        float y4 = (line2_end.x * sin(item2->rotation) +
                                        line2_end.y * cos(item2->rotation)) + item2->loc.y;

			//check if the lines are even near each other
			if (x1 > x2) {
				if (x3 > x1 && x4 > x1) continue;
				if (x3 < x2 && x4 < x2) continue;
			} else {
				if (x3 > x2 && x4 > x2) continue;
				if (x3 < x1 && x4 < x1) continue;
			}


			//equations shamefully yoinked from wikipedia
			//could probably be better

			float px, py;
			px = ((x1*y2 - y1*x2) * (x3 - x4) - (x1 - x2) * (x3*y4 - y3*x4))/
				((x1 - x2) * (y3 - y4) - (y1 - y2)*(x3 - x4));
			py = ((x1*y2 - y1*x2) * (y3 - y4) - (y1 - y2) * (x3*y4 - y3*x4))/
                                ((x1 - x2) * (y3 - y4) - (y1 - y2)*(x3 - x4));

			//check if these x and y values are within both ranges

			if ((px > x1 && px > x2) || (px < x1 && px < x2)) continue;
			if ((py > y1 && py > y2) || (py < y1 && py < y2)) continue;

			if ((px > x3 && px > x4) || (px < x3 && px < x4)) continue;
                        if ((py > y3 && py > y4) || (py < y3 && py < y4)) continue;
			//we know for sure these lines collide, and where it is.


			if (final->count != 0) {
				final->count++;
				//printf("count is %d\n",final->count);
				//printf("reallocating %p to %ld\n",final->overlaps,sizeof(vec)*final->count);
				vec * tmp = realloc(final->overlaps,sizeof(vec)*final->count);
				if (tmp == NULL) {
					printf("error reallocating collision space with %d vectors\n",final->count);
					return;
				}
				final->overlaps = tmp;
				final->overlaps[final->count-1].x = px;
				final->overlaps[final->count-1].y = py;
			} else {
				final->count = 1;
				//printf("count is 1\n");
				final->overlaps = calloc(1,sizeof(vec));
				final->overlaps[0].x = px;
				final->overlaps[0].y = py;
			}

		}
	}
	if (collision_item == NULL) {
		collision_item = final;
		collision_first = final;
		collision_item_end = final;
	} else {
		collision_item_end->next = final;
		collision_item_end = final;
	}
}

void calc_forces() {

	//for each collision,
	//calculate the action and re-action
	//and apply the force to the object

	while (1) {
		if (collision_item == NULL) break;

		if (collision_item->count == 0) {
			printf("invalid collision\n");
			collision_item = collision_item->next;
			continue;
		}

		vec avg = collision_item->overlaps[0];

		if (collision_item->count > 1) {
			int index;
			for (index = 1; index < collision_item->count; index++) {
				avg.x = (avg.x + collision_item->overlaps[index].x)/2;
				avg.y = (avg.y + collision_item->overlaps[index].y)/2;
			}
		}


	object * item1 = collision_item->object1;
	object * item2 = collision_item->object2;

        //calculate the force of object 1 on the point
        //dim velocity + angular velocity on dimension

	//probably should be using center of mass, not loc. TODO
	float object1_distance = sqrt(pow(avg.x - item1->loc.x,2) + pow(avg.y - item1->loc.y,2));
	vec vel1;
	vel1.x = item1->velocity.x + (item1->ang_vel * object1_distance * PI * cos(item1->rotation));
	vel1.y = item1->velocity.y + (item1->ang_vel * object1_distance * PI * sin(item1->rotation));

        //calculating the force of object 2 on the point
        //dim velocity + angular velocity on dimension


	float object2_distance = sqrt(pow(avg.x - item2->loc.x,2) + pow(avg.y - item2->loc.y,2));
        vec vel2;
        vel2.x = item2->velocity.x + (item2->ang_vel * object2_distance * PI * cos(item2->rotation));
        vel2.y = item2->velocity.y + (item2->ang_vel * object2_distance * PI * sin(item2->rotation));

        //now that we know the 2 forces, find the net force

	//TODO: calculating rotational intertia is totally wrong. re-do
	//properly for rotational inertia
	vec force1;
	vec force2;
	force1.x = vel1.x * item1->mass;
	force1.y = vel1.y * item1->mass;

	force2.x = vel2.x * item2->mass;
	force2.y = vel2.y * item2->mass;


        //calculate the force on the center of mass for each
        //object and apply it

	//TODO: make vectors an actual useful datatype
	//eg. vec1 = vec1 + vec2;
	//can this be done in standard C?
	vec avg_rel1 = avg; //average collision relative to object1
	avg_rel1.x -= item1->loc.x;
	avg_rel1.y -= item1->loc.y;

	vec avg_rel2 = avg;
	avg_rel2.x -= item2->loc.x;
	avg_rel2.y -= item2->loc.y;

	//calculate translational and angular velocity

	float normal1;
	if (avg_rel1.x != 0)
		normal1 = atan(avg_rel1.y/avg_rel1.x);
	else normal1 = PI/2;
	float force_angle1;
	if (force1.x != 0)
		force_angle1 = atan(force1.y/force1.x);
	else force_angle1 = 0;
	float diff_angle1 = force_angle1 - normal1;

        float normal2;
	if (avg_rel2.x != 0)
		normal2 = atan(avg_rel2.y/avg_rel2.x);
	else normal2 = PI/2;
        float force_angle2;
	if (force2.x != 0)
		force_angle2 = atan(force2.y/force2.x);
	else force_angle2 = 0;
        float diff_angle2 = force_angle2 - normal2;

	//translate these back to their real objects
	item1 = object_array[item1->index];
	item2 = object_array[item2->index];

	//apply the sin of the magnitude divided by mass to the translational velocity

	//trans_vel is the translational velocity along the normal angle
	float trans_vel1 = (sqrt(pow(force1.x,2) + pow(force1.y,2)) * sin(diff_angle1))/item1->mass;
	item1->velocity.x -= trans_vel1 * cos(normal1);
	item1->velocity.y -= trans_vel1 * sin(normal1);

        trans_vel1 = (sqrt(pow(force1.x,2) + pow(force1.y,2)) * sin(diff_angle1))/item2->mass;
        item2->velocity.x += trans_vel1 * cos(normal1);
        item2->velocity.y += trans_vel1 * sin(normal1);

        float trans_vel2 = (sqrt(pow(force2.x,2) + pow(force2.y,2)) * sin(diff_angle2))/item2->mass;
        item2->velocity.x -= trans_vel2 * cos(normal2);
        item2->velocity.y -= trans_vel2 * sin(normal2);

        trans_vel2 = (sqrt(pow(force2.x,2) + pow(force2.y,2)) * sin(diff_angle2))/item1->mass;
        item1->velocity.x += trans_vel2 * cos(normal2);
        item1->velocity.y += trans_vel2 * sin(normal2);


	/*apply the cos of the magnitude divided by interia frame for the rotational velocity
	 *I=(M^2)/2 for a rudimentry solid cylender.
	 *there is probably a much better way to do this.
	 */

	//trans_vel is now the force applied to rotation
	trans_vel1 = (force_angle1 * cos(diff_angle1)) / (pow(item1->mass,2)/2);
	//calculate the diameter of the object
	float dia1 = sqrt( pow(avg_rel1.x,2) + pow(avg_rel1.y,2));
	item1->ang_vel += trans_vel1 / (dia1 * PI);

	trans_vel2 = (force_angle2 * cos(diff_angle2)) / (pow(item2->mass,2)/2);
	float dia2 = sqrt( pow(avg_rel2.x,2) + pow(avg_rel2.y,2));
	item2->ang_vel += trans_vel2 / (dia2 * PI);


	//on to the next!
	collision_item = collision_item->next;
	}

	//TODO: calculate re-action forces for objects in a group
	//objects break at this point.
}

void apply_tick(float time, object * item) {
	item->loc.x = item->loc.x + (item->velocity.x * time);
	item->loc.y = item->loc.y + (item->velocity.y * time);
	item->rotation = item->rotation + (item->ang_vel * time);
}

//input temp pointer must hold at least 2 values
void get_object_velocity(int object_id, float * temp) {

	if (check_id(object_id)) {
		temp[0] = object_array[object_id]->velocity.x;
		temp[1] = object_array[object_id]->velocity.y;
	}
}

//returns array of {angle,distance}
void get_object_velocity_as_vec(int object_id, float * temp) {

	if (!check_id(object_id)) {
		memset(temp,0,sizeof(float)*2);
	} else {
		object * item = object_array[object_id];

		// r = sqrt(x^2+y^2)
		temp[1] = sqrtf(pow(item->loc.x,2) + pow(item->loc.y,2));

		// x = r*cos(theta)
		// x/r = cos(theta)
		// arccos(x/r) = theta
		temp[0] = (float)  acos(item->loc.x/temp[1]);

	}

}

float get_object_mass(int object_id) {

	if (check_id(object_id)) {
		return object_array[object_id]->mass;
	}
	return 0;
}

void get_object_location(int object_id, float * temp) {
	if (check_id(object_id)) {
		temp[0] = object_array[object_id]->loc.x;
		temp[1] = object_array[object_id]->loc.y;
	}
}

int get_object_shape(int object_id) {
	if (check_id(object_id)) {
		return object_array[object_id]->object_shape;
	}
	return 0;
}

int get_object_has_gravity(int object_id) {
	if (check_id(object_id)) {
		return object_array[object_id]->gravity;
	}
	return 0;
}

void get_object_mass_center(int object_id, float * loc){
	if(check_id(object_id)) {
		loc[0] = object_array[object_id]->mass_center.x;
		loc[1] = object_array[object_id]->mass_center.y;
	}
	memset(loc,0,sizeof(float)*2);

}

int get_object_links(int object_id, int ** link_array) {
	if(check_id(object_id)) {
		if (object_array[object_id]->link_group == 0) return 0;
		*link_array = object_array[object_id]->link_group->objects;
		return object_array[object_id]->link_group->size;

	}
	return 0;

}

int get_object_collides(int object_id) {

	if (check_id(object_id)) {
		return object_array[object_id]->collides;
	}
	return 0;


}

void set_object_velocity(int object_id, float x, float y) {

	if (check_id(object_id)) {
		object_array[object_id]->velocity.x = x;
		object_array[object_id]->velocity.y = y;
	}

}

void set_object_velocity_by_vec(int object_id, float angle, float speed) {

	if (check_id(object_id)) {
		float x,y;

		x = speed * cos(angle);
		y = speed * sin(angle);

		object_array[object_id]->velocity.x = x;
		object_array[object_id]->velocity.y = y;
	}

}

void set_object_mass(int object_id, float mass) {

	if (check_id(object_id)) {
		object_array[object_id]->mass = mass;
	}

}

void set_object_location(int object_id, float x, float y) {

	if (check_id(object_id)) {
		object_array[object_id]->loc.x = x;
		object_array[object_id]->loc.y = y;
	}

}

void set_object_shape(int object_id, int shape_id) {

	if (check_id(object_id)) {
		if (shape_id <= shape_array_size)
		object_array[object_id]->object_shape = shape_id;
	}

}

void set_object_has_gravity(int object_id, int bool) {

	if (check_id(object_id)) {
		object_array[object_id]->gravity = bool;
	}

}

void set_object_mass_center(int object_id, float x,float y) {

	if (check_id(object_id)) {
		object_array[object_id]->mass_center.x = x;
		object_array[object_id]->mass_center.y = y;
	}

}

void set_object_collides(int object_id, int bool) {

	if (check_id(object_id)) {
		object_array[object_id]->collides = bool;
	}

}

void set_object_link(int object_id1 ,int object_id2) {
	//TODO
}

void del_object_link(int object_id1 ,int object_id2) {
	//todo
}
//TODO: del_shape?
void del_object(int object_id) {
	if (check_id(object_id)) {
		if (empty_size == 0) {
			empty_slots = calloc(1,sizeof(int));
			empty_slots[0] = object_id;
		} else {
			int * tmp_slots;
			tmp_slots = realloc(empty_slots,sizeof(int)*(empty_size+1));
			tmp_slots[empty_size] = object_id;
			empty_slots = tmp_slots;
		}
		empty_size++;
		memset(object_array[object_id],0,sizeof(object));

	}
}

//creates a new shape for the shape array
//return the index to the newly created blank
//shape. if memory allocation fails, it returns -1.

int start_shape() {

	if (shape_array_size == 0) {
		//initialize shape array
		shape_array = calloc(1,sizeof(shape*));
		shape * tmp = calloc(1,sizeof(shape));
		if (shape_array != 0 && tmp != 0) {
			shape_array_size = 1;
			shape_array[0] = tmp;
		} else {
			printf("failed to initialize shape array in fizziks\n");
			return -1;
		}

	} else {
		shape_array = realloc(shape_array,sizeof(shape *)*(shape_array_size+1));
		shape * tmp = calloc(1,sizeof(shape));
		if (shape_array != NULL && tmp != NULL) {
			shape_array[shape_array_size] = tmp;
			shape_array_size += 1;

		} else {
			printf("failed to add shape to array\n");
			return -1;
		}
	}
	//return the index to the element we just added
	return shape_array_size-1;

}
//adds a point to the list of points belonging to a shape.
//TODO: point list needs to be organied clockwise,
//i should make a sanity checker to add points after
//shape array is created.
void add_point_to_shape(int shape_id ,float x, float y) {
	if (shape_array_size < shape_id) {
		printf("attempted access to non-existant shape\n");
		return;
	}
	shape * our_shape = shape_array[shape_id];

	if (our_shape->size == 0) {
		//initializing shape array
		our_shape->points = calloc(1,sizeof(vec));
		if (our_shape->points == 0) {
			printf("failed to allocate space for new point on shape\n");
			return;
		}
		our_shape->size++;
		our_shape->points[0].x = x;
		our_shape->points[0].y = y;
		//successfully created shape array and added point to it
	} else {
		//adding point to existing array
		vec * tmp = realloc(our_shape->points,(our_shape->size + 1)*sizeof(vec));
		if (tmp == 0) {
			printf("failed to allocate additional space for shape\n");
		}
		our_shape->points = tmp;
		our_shape->points[our_shape->size].x = x;
		our_shape->points[our_shape->size].y = y;
		our_shape->size++;
		//successfully added point to existing shape array
	}

}

void add_object_velocity_at_point(int object_id, float x_loc, float y_loc,
						 float width ,float height) {

	if (!check_id(object_id)) return;
	object * item = object_array[object_id];
	vec angle_rel = item->loc;
	angle_rel.x -= x_loc;
	angle_rel.y -= y_loc;

	float normal = atan(angle_rel.y/angle_rel.x);
	float force_angle = atan(height/width);
	float diff_angle = force_angle - normal;

	float trans_vel = (sqrt(pow(width,2) + pow(height,2)) * sin(diff_angle))/item->mass;
        item->velocity.x += trans_vel * cos(normal);
        item->velocity.y += trans_vel * sin(normal);

	float dia = sqrt( pow(angle_rel.x,2) + pow(angle_rel.y,2));
        item->ang_vel += trans_vel / (dia * PI);

}

void add_object_velocity_at_point_angle(int object_id, float x_loc ,float y_loc,
							  float angle ,float speed) {
	add_object_velocity_at_point(object_id,x_loc,y_loc,
					   speed * cos(angle), speed * sin(angle));

}

int create_object(int shape_id) {
	//TODO: have it fill empty slots in the pointer array before expanding and apprending
	if (shape_id >= shape_array_size) {
		printf("attempted use of nonexistant shape to create object\n");
	}
	if (object_array_size == 0) {

		object_array = calloc(1,sizeof(object *));
		object * tmp = calloc(1,sizeof(object));
		if (object_array != 0 && tmp != 0) {
			object_array_size = 1;
			tmp->object_shape = shape_id;
			tmp->index = 0;
			object_array[0] = tmp;
			return 0;
		} else {
			printf("error creating object array\n");
			return -1;
		}
	} else {
		object ** tmp_array = realloc(object_array,(object_array_size+1) * sizeof(object *));
		if (tmp_array != 0) {
			object_array = tmp_array;
			object * tmp = calloc(1,sizeof(object));
			if (tmp == 0) {
				printf("error creating new object for array\n");
				return -1;
			}
			object_array[object_array_size] = tmp;
			object_array[object_array_size]->object_shape = shape_id;
			object_array[object_array_size]->index = object_array_size;
			object_array_size += 1;
			return object_array_size -1;
		} else {
			printf("error expanding object array\n");
			return -1;
		}
	}

}



