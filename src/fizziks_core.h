#ifndef fizziks_core
#define fizziks_core
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

typedef struct vec vec;
typedef struct shape shape;
typedef struct object object;
typedef struct object_group object_group;
typedef struct collision collision;

int init(float);
void do_tick_in_rect(int, float, float, float, float);
void get_object_collisions(collision **); // fetch first element of collision linked list


// internal methods
void pretend_tick(int, float, float, float, float);
void detect_overlaps();
void overlap_compare(object*,object*);
void calc_forces();
void real_tick(int, float, float, float, float);
void apply_tick(float,object *);

//retriever methods
void get_object_velocity(int, float *);
void get_object_velocity_as_vec(int, float *);
float get_object_mass(int);
void get_object_location(int, float *);
int get_object_shape(int);
int get_object_has_gravity(int); //return boolean
void get_object_mass_center(int, float *); //returns center in float *
//for retrieving links, pass a pointer to an int *
//which will be redirected to the list of pointers
//and will return the # of points
int get_object_links(int, int **);
int get_object_collides(int); //return boolean


//set methods
void set_object_velocity(int, float, float);
void set_object_velocity_by_vec(int, float, float);
void set_object_mass(int, float);
void set_object_location(int, float, float);
void set_object_shape(int, int);
void set_object_has_gravity(int, int);
void set_object_mass_center(int,float,float);
void set_object_collides(int, int);
void set_object_link(int,int);
void del_object_link(int,int);
void del_object(int);

//shape defining
int start_shape();
void add_point_to_shape(int,float,float);

//manual forces
void add_object_velocity_at_point(int,float,float,float,float);
void add_object_velocity_at_point_angle(int,float,float,float,float);

int create_object(int);


//definition for a vector
struct vec {
	//array of 2 components for x and y
    float x;
	float y;

};

//definition for a shape
//that a game world object can use
struct shape {
        int size;
	//variable length array of points along
	//the shape running clockwise
        vec * points;
};

//definition for an object in the game world
struct object {
	int index;
	//absolute location for the center of mass
        vec loc;

	//relative location for center of mass
	vec mass_center;

	//id of the shape for this object
	int object_shape;

	object_group * link_group;

	float rotation;
	float mass;
	vec velocity;
	float ang_vel;
	int gravity;
	int collides;
};

//definition for a group of objects
struct object_group {

	int group;
	int size;
	int * objects;

};
//

//linked list element for collisions
struct collision {
	vec * overlaps;
	int count;
	object * object1;
	object * object2;
	collision * next;
};

#endif
