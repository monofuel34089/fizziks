# Fizziks

Really old C code I wrote when learning C to see if I could make a working physics engine,
probably does not work well and does not include a warranty

Fizziks is limited to 2D convex rigid body physics. Objects can be linked together
to form larger rigid objects. during runtime, rectangles of the sandbox will be simulated
for a given time in milliseconds. you can designate the entire sandbox to perform a tick,
or you can designate ticks in various areas to perform at varying intervals. eg: area
in the middle of the screen does 1 tick every 30ms, while areas not on-screen are only
simulated every 100ms.

objects are designated by a creating a shape and adding points to the shape.

I think that concave objects (with 'interior' sides) could be complicated to handle, might be
easier to make objects built up of several convex objects rather than a concave object.

TODO:

- prevent high-speed objects from teleporting through each other.
- switch to integers and events to make it easier to record and replay simulations?
- is it worthwile to simulate rectangles of the world, rather than just tick the entire world at once?


how to build:

make run
	build the library and run tests

make sharedlib
	create the shared library in /lib

make clean
	remove built libraries and object files
